<div class="students index" style="background: url(../img/school.jpg)">
    <h2><?php echo __('Students'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('Roll No'); ?></th>
            <th><?php echo $this->Paginator->sort('Name'); ?></th>
            <th><?php echo $this->Paginator->sort('Class'); ?></th>
            <th><?php echo $this->Paginator->sort('Father\'s Name'); ?></th>
            <th><?php echo $this->Paginator->sort('local_guide'); ?></th>
            <th><?php echo $this->Paginator->sort('phone_no'); ?></th>
            <th><?php echo $this->Paginator->sort('address'); ?></th>
            <th><?php echo $this->Paginator->sort('email'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($students as $student): ?>
        <tr>
            <td><?php echo $this->html->link($student['Student']['roll_no'],array('action'=>'view', $student['Student']['studentID'])); ?>&nbsp;</td>
            <td><?php echo h($student['Student']['name']); ?>&nbsp;</td>
            <td><?php echo $this->html->link($student['Studclass']['class_name'], array('controller' => 'studclasses', 'action'=>'view', $student['Studclass']['classID'])); ?>&nbsp;</td>
            <td><?php echo h($student['Student']['fname']); ?>&nbsp;</td>
            <td><?php echo h($student['Student']['local_guide']); ?>&nbsp;</td>
            <td><?php echo h($student['Student']['phone_no']); ?>&nbsp;</td>
            <td><?php echo h($student['Student']['address']); ?>&nbsp;</td>
            <td><?php echo h($student['Student']['email']); ?>&nbsp;</td>

            <td><?php if($student['Student']['status']=='1'){
                echo $this->Html->image('true.png');
            }else{
                echo $this->Html->image('false.png');
            }; ?>&nbsp;</td>

            <td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $student['Student']['studentID'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $student['Student']['studentID'])); ?>

            </td>
        </tr>
<?php endforeach; ?>
    </table>

</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' => 'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' => 'payment_details', 'action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances','action' => 'make')); ?></li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances','action' => 'index')); ?></li>-->
    </ul>    
</div>
