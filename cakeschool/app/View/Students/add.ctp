<div class="students form">
<?php echo $this->Form->create('Student'); ?>
    <fieldset>
        <legend><?php echo __('Add Student'); ?></legend>
	<?php
		echo $this->Form->input('roll_no');
		echo $this->Form->input('name');
                echo $this->Form->input('student_classID', array('label' => 'Class', 'options' =>$studentclass));
		echo $this->Form->input('fname', array('label' => 'Father\'s Name'));
		echo $this->Form->input('local_guide', array('label' => 'Local Guidence'));
		echo $this->Form->input('phone_no');
		echo $this->Form->input('address');
		echo $this->Form->input('email');
		echo $this->Form->input('status', array('type' => 'checkbox'));
	?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' => 'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers','action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' => 'payment_details', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' => 'payment_details', 'action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances','action' => 'make')); ?></li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances','action' => 'index')); ?></li>-->
    </ul>
</ul>
</div>
