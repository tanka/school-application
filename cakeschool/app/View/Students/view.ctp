<div class="students view">
    <h2><?php echo __('Student'); ?></h2>
    <dl>
        <dt><?php echo __('Roll No'); ?></dt>
        <dd>
			<?php echo h($student['Student']['roll_no']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Name'); ?></dt>
        <dd>
			<?php echo h($student['Student']['name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Class'); ?></dt>
        <dd>
			<?php echo h($student['Studclass']['class_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Father\'s Name'); ?></dt>
        <dd>
			<?php echo h($student['Student']['fname']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Local Guide'); ?></dt>
        <dd>
			<?php echo h($student['Student']['local_guide']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Phone No'); ?></dt>
        <dd>
			<?php echo h($student['Student']['phone_no']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Address'); ?></dt>
        <dd>
			<?php echo h($student['Student']['address']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Email'); ?></dt>
        <dd>
			<?php echo h($student['Student']['email']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Status'); ?></dt>
        <dd>
			<?php if($student['Student']['status']=='1'){
                            echo $this->Html->image('true.png');
                        }else{
                           echo $this->Html->image('false.png'); 
                        }; ?>
            &nbsp;
        </dd>
    </dl>
    <br />
    <br />
    <div class="related">
        <h3><?php echo __('Related Payment Details'); ?></h3>
	<?php if (!empty($student['StudentsPayment'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Date'); ?></th>
                <th><?php echo __('Amount'); ?></th>
                <th><?php echo __('Comment'); ?></th>
            </tr>
	<?php foreach ($student['StudentsPayment'] as $studentsPayment): ?>
            <tr>
                <td><?php echo $studentsPayment['date']; ?></td>
                <td><?php echo $studentsPayment['paid_amt']; ?></td> 
                <td><?php echo $studentsPayment['comment']; ?></td> 
            </tr>
	<?php endforeach; ?>
        </table>
<?php endif; ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' => 'payment_details', 'action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances','action' => 'make')); ?></li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances','action' => 'index')); ?></li>-->
    </ul>
</div>

