<div class="attendances form">
    <h2><?php echo __('Attendances'); ?></h2>
    <h3><?php echo date('d-m-Y'); ?></h3>
    <table cellpadding="0" cellspacing="0">
           <?php  //echo $this->Form->create('Attendance', array('action' => 'attended')); ?>
        <tr>
            <th><?php echo __('Teachers Attendance'); ?></th>
             <th><?php echo __('Teachers Identity No'); ?></th>
        </tr>  
      <?php echo $this->Form->create('Attendance');
             foreach ($attendances as $i => $attendance): 
            ?>
        <tr>
           <?php 
             echo $this->Form->input('Attendance.'.$i.'.attendance_teacherID', array('type' => 'hidden', 'value' => $attendance['Teacher']['teacherID']));
          
           echo $this->Form->input('Attendance.'.$i.'.attendance_date', array('type' => 'hidden', 'value' => date('Y-m-d')));
             ?>
            <td><?php echo ($attendance['Teacher']['teacher_name']);?>&nbsp;
                <?php echo $this->Form->checkbox('Attendance.'.$i.'.attendance', array()); ?>&nbsp;</td>
             <td><?php echo ($attendance['Teacher']['teacher_cid']);?>&nbsp;
        </tr>
        <?php 
         endforeach;
         ?>
        </table>
        <?php echo $this->Form->end(__('Submit')); ?>
        
   
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('View Attendance'), array('action' => 'index')); ?></li>
        <br/>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' =>'payment_details','action' => 'viewparticular')); ?> </li>
    </ul>
</div>