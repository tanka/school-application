<div class="attendances form">
   <?php echo $this->Form->create('Attendance', array('action'=>'index')); ?>
    <fieldset>
        <legend><?php echo __('View Attendance Detail'); ?></legend>
	<?php echo $this->Form->input('attendance_date', array('label' =>'Date', 'type'=>'date')); ?>
        <?php echo $this->Form->end(__('View Attendance')); ?>
    </fieldset>
    
    <?php if($attendances=='0') {?>
    <div></div>
    <?php }else{ ?>
    
        
    
	<h2><?php echo __('Attendances'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo __('Teacher Name'); ?></th>
                        <th><?php echo __('Teacher Identity No'); ?></th>
			<th><?php echo __('attendance_date'); ?></th>
			<th><?php echo __('attendance'); ?></th>
	</tr>
       	<?php foreach ($attendances as $attendance): ?>
	<tr>
            <td><?php echo h($attendance['Teacher']['teacher_name']); debug($attendance) ?>&nbsp;</td>
                <td><?php echo h($attendance['Teacher']['teacher_cid']); ?>&nbsp;</td>
		<td><?php echo h($attendance['Attendance']['attendance_date']); ?>&nbsp;</td>
		<td><?php
                if($attendance['Attendance']['attendance']=='0'){ 
                    echo $this->Html->image('cross.png', array('alt' => 'Absent'));
                }
                else
                {
                     echo $this->Html->image('tick.png', array('alt' => 'Present'));
                }
                 ?>&nbsp;</td>
        
        	</tr>
                <?php endforeach; ?>
               
	</table>
	 <?php }?>  
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('action' => 'make')); ?></li>-->
        <br/>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>   
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
<!--        <li><?php echo $this->Html->link(__('List Attendances'), array('controller' =>'attendances','action' => 'index')); ?></li>-->
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' =>'payment_details','action' => 'viewparticular')); ?> </li>
    </ul>
</div>
