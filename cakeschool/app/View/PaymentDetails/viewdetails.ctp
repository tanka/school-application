<div class="paymentDetails form">
<?php echo $this->Form->create('PaymentDetails', array('action'=>'viewdetails')); ?>
    <fieldset>
        <legend><?php echo __('Add Detail'); ?></legend>
	<?php echo $this->Form->input('Student.roll_no', array('label' =>'Roll No')); ?>
        <?php echo $this->Form->end(__('View Details')); ?>
    </fieldset>
    <?php if(empty($studentPayments)){?>
    <div><?php 
           echo '<h2> Enter the Valid Roll Number'; ?></div>
    <?php }else{ ?>
    <div>
        <dl>
            <dt><?php echo __('Name');?></dt>
            <dd><?php echo h($studentPayments['Student']['name']); ?>&nbsp;</dd>
            <dt><?php echo __('Class');?></dt>
            <dd><?php echo h($studentPayments['Studclass']['class_name']); ?>&nbsp;</dd>
            <dt><?php echo __('Father\'s Name'); ?></dt>
            <dd><?php echo h($studentPayments['Student']['fname']); ?>&nbsp;</dd>
            <dt><?php echo __('Local Guidence'); ?></dt>
            <dd><?php echo h($studentPayments['Student']['local_guide']); ?>&nbsp;</dd>
            <dt><?php echo __('Phone No'); ?></dt>
            <dd><?php echo h($studentPayments['Student']['phone_no']); ?>&nbsp;</dd>
            <dt><?php echo __('Address'); ?></dt>
            <dd><?php echo h($studentPayments['Student']['address']); ?>&nbsp;</dd>
            <dt><?php echo __('Email'); ?></dt>
            <dd><?php echo h($studentPayments['Student']['email']); ?>&nbsp;</dd>
        </dl>

    </div>

    <br />
    <br />
                 <?php echo $this->Form->create('Paymentdetail', array('action'=>'add')); ?>
    <fieldset>
        <legend><?php echo __('Enter Payment Details'); ?></legend>
           <?php
            //    debug($studentPayments);
                echo $this->Form->hidden('student_paymentID', array('value' => $studentPayments['Student']['studentID']));
                echo $this->Form->hidden('name', array('value' => $studentPayments['Student']['name']));
                echo $this->Form->hidden('roll_no', array('value' => $studentPayments['Student']['roll_no']));
                echo $this->Form->hidden('phone_no', array('value' => $studentPayments['Student']['phone_no']));
                echo $this->Form->hidden('email', array('value' => $studentPayments['Student']['email']));
		echo $this->Form->input('date');
		echo $this->Form->input('paid_amt', array('label' => 'Amount'));
                echo $this->Form->input('comment',array('label' => 'Comment', 'type'=>'textarea'));
            ?>      
            <?php echo $this->Form->end(__('Proceed for Payment')); ?>
    </fieldset>  
            <?php }?>  
</div>       

<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View  Particular Student'), array('action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances','action' => 'make')); ?></li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances','action' => 'index')); ?></li>-->
    </ul>
</div>

