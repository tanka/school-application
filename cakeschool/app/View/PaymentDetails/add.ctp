<div class="paymentDetails form">

    <h2><?php echo __('Payment Slip'); ?></h2> 


    <div>
        <dl>
            <dt><?php echo __('Name');?> </dt>
            <dd><?php echo $this->data['Paymentdetail']['name']; ?></dd>

            <dt><?php echo __('Roll Number');?> </dt>
            <dd><?php echo $this->data['Paymentdetail']['roll_no']; ?></dd>

            <dt><?php echo __('Phone Number');?> </dt>
            <dd><?php echo $this->data['Paymentdetail']['phone_no']; ?></dd>

            <dt><?php echo __('Email');?> </dt>
            <dd><?php echo $this->data['Paymentdetail']['email']; ?></dd>

            <dt><?php echo __('Date');?></dt>
            <dd><?php echo h($this->data['Paymentdetail']['date']['day'].'/'. $this->data['Paymentdetail']['date']['month'].'/'. $this->data['Paymentdetail']['date']['year']); ?></dd>

            <dt><?php echo __('Amount Paid');?></dt>
            <dd><?php echo h($this->data['Paymentdetail']['paid_amt']); ?></dd>

            <dt><?php echo __('Comment');?></dt>
            <dd><?php echo h($this->data['Paymentdetail']['comment']); ?>&nbsp;</dd>

        </dl>

    </div>  
</div>       

<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances','action' => 'make')); ?></li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances','action' => 'index')); ?></li>-->
    </ul>
</div>
