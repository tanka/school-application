<div class="paymentDetails view">
    <h2><?php echo __('Detail Description'); ?></h2>
<?php echo $this->Form->create('Particulars'); 
      echo $this->Form->input('Student.roll_no', array('label' => __('Roll No')));	
    echo $this->Form->end(__('View')); 
?>


<?php if(empty($studentDetails)) {?>
    <div><?php echo '<h2>Enter the Valid Roll Number</h2>'; ?></div>
<?php }else{ ?>
    <div>
        <dl>
            <dt><?php echo __('Name');?></dt>
            <dd><?php echo h($studentDetails['Student']['name']); ?>&nbsp;</dd>
            <dt><?php echo __('Class');?></dt>
            <dd><?php echo h($studentDetails['Studclass']['class_name']); ?>&nbsp;</dd>
            <dt><?php echo __('Father\'s Name'); ?></dt>
            <dd><?php echo h($studentDetails['Student']['fname']); ?>&nbsp;</dd>
            <dt><?php echo __('Local Guidence'); ?></dt>
            <dd><?php echo h($studentDetails['Student']['local_guide']); ?>&nbsp;</dd>
            <dt><?php echo __('Phone No'); ?></dt>
            <dd><?php echo h($studentDetails['Student']['phone_no']); ?>&nbsp;</dd>
            <dt><?php echo __('Address'); ?></dt>
            <dd><?php echo h($studentDetails['Student']['address']); ?>&nbsp;</dd>
            <dt><?php echo __('Email'); ?></dt>
            <dd><?php echo h($studentDetails['Student']['email']); ?>&nbsp;</dd>
        </dl>
    </div>  
    <br />
    <br />
    <div class="related">
        <h3><?php echo __('Payment Details'); ?></h3>
	<?php if (!empty($studentDetails['StudentsPayment'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Date'); ?></th>
                <th><?php echo __('Amount'); ?></th>
                <th><?php echo __('Comment'); ?></th>
            </tr>
	<?php foreach ($studentDetails['StudentsPayment'] as $studentsPayment): ?>
            <tr>
                <td><?php echo $studentsPayment['date']; ?></td>
                <td><?php echo $studentsPayment['paid_amt']; ?></td>
                <td><?php echo $studentsPayment['comment']; ?></td>
            </tr>
	<?php endforeach; ?>
        </table>
<?php endif; ?>


    </div>
  <?php }?>  
</div> 

<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances','action' => 'make')); ?></li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances','action' => 'index')); ?></li>-->
    </ul>
</div>


