<div class="teachers index">
    <h2><?php echo __('Teachers'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('CID No'); ?></th>
            <th><?php echo $this->Paginator->sort('local_address'); ?></th>
            <th><?php echo $this->Paginator->sort('permanent _address'); ?></th>
            <th><?php echo $this->Paginator->sort('phone_no'); ?></th>
            <th><?php echo $this->Paginator->sort('email'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($teachers as $teacher): ?>
        <tr>
            <td><?php echo h($teacher['Teacher']['teacher_name']); ?>&nbsp;</td>
            <td><?php echo h($teacher['Teacher']['teacher_cid']); ?>&nbsp;</td>
            <td><?php echo h($teacher['Teacher']['local_address']); ?>&nbsp;</td>
            <td><?php echo h($teacher['Teacher']['permanent _address']); ?>&nbsp;</td>
            <td><?php echo h($teacher['Teacher']['phone_no']); ?>&nbsp;</td>
            <td><?php echo h($teacher['Teacher']['email']); ?>&nbsp;</td>
            <td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $teacher['Teacher']['teacherID'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $teacher['Teacher']['teacherID'])); ?>
            </td>
        </tr>
<?php endforeach; ?>
    </table>

</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
   <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' => 'payment_details', 'action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances', 'action' => 'make')); ?> </li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances', 'action' => 'index')); ?> </li>-->
    </ul>
</div>
