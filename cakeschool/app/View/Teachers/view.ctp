<div class="teachers view">
    <h2><?php echo __('Teacher'); ?></h2>
    <dl>
        <dt><?php echo __('Name'); ?></dt>
        <dd>
			<?php echo h($teacher['Teacher']['teacher_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('CID No'); ?></dt>
        <dd>
			<?php echo h($teacher['Teacher']['teacher_cid']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Local Address'); ?></dt>
        <dd>
			<?php echo h($teacher['Teacher']['local_address']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Permanent  Address'); ?></dt>
        <dd>
			<?php echo h($teacher['Teacher']['permanent _address']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Phone No'); ?></dt>
        <dd>
			<?php echo h($teacher['Teacher']['phone_no']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Email'); ?></dt>
        <dd>

			<?php echo h($teacher['Teacher']['email']); ?>
            &nbsp;
        </dd>
        <br />
       
    </dl>
    <br />
    <br />
    <h1><?php echo __('Class Assigned:'); ?></h1> 
            <?php foreach($teacher['TeachersClass'] as $classes){?>
    <dl>
        <dt><?php echo h($classes['class_name']); ?></dt>
    </dl>  
        <?php } ?>

</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' => 'payment_details', 'action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances', 'action' => 'make')); ?> </li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances', 'action' => 'index')); ?> </li>-->
    </ul>
</div>
