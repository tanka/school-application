<div class="studclasses view">
    <h2><?php echo __('Class'); ?></h2>
    <dl>
        <dt><?php echo __('Class Name'); ?></dt>
        <dd>
			<?php echo h($studclass['Studclass']['class_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Class Teacher'); ?></dt>
        <dd>
			<?php echo h($studclass['Teacher']['teacher_name']); ?>
            &nbsp;
        </dd>

    </dl>
    <br />
    <br />
    <div class="related">
        <h3><?php echo __('Subject List'); ?></h3>
        <?php if(!empty($studclass['Subject'])){?>
        <?php foreach($studclass['Subject'] as $subject):  ?>
        <dl>
            <dt><?php echo $subject['subject_name']; ?></dt>
        </dl>
        <?php endforeach; ?>
        <?php }else{
            echo '<h2> No Subjects list for this Class</h2>';
        } ?>
    </div>
    <br />
    <br />
    <div class="related">
        <h3><?php echo __('Related Students'); ?></h3>
	<?php if (!empty($studclass['StudentsClass'])){ ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Roll No'); ?></th>
                <th><?php echo __('Name'); ?></th>
                <th><?php echo __('Fname'); ?></th>
                <th><?php echo __('Local Guide'); ?></th>
                <th><?php echo __('Phone No'); ?></th>
                <th><?php echo __('Address'); ?></th>
                <th><?php echo __('Email'); ?></th>
                <th><?php echo __('Status'); ?></th>              
            </tr>
	<?php foreach ($studclass['StudentsClass'] as $studentsClass): ?>
            <tr>
                <td><?php echo $studentsClass['roll_no'];?></td>
                <td><?php echo $studentsClass['name']; ?></td>
                <td><?php echo $studentsClass['fname']; ?></td>
                <td><?php echo $studentsClass['local_guide']; ?></td>
                <td><?php echo $studentsClass['phone_no']; ?></td>
                <td><?php echo $studentsClass['address']; ?></td>
                <td><?php echo $studentsClass['email']; ?></td>
                <td><?php if($studentsClass['status']=='1'){
                echo $this->Html->image('true.png');
            }else{
                echo $this->Html->image('false.png');
            }; ?>&nbsp;</td>
            </tr>
	<?php endforeach; ?>
        </table>
        <?php }else{
            echo '<h2>No Students for this Class</h2>';
            
        } ?>
    </div>
</div>


<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' => 'payment_details', 'action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances','action' => 'make')); ?></li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances','action' => 'index')); ?></li>-->
    </ul>
</div>

