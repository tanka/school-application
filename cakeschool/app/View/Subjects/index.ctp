<div class="subjects index">
    <h2><?php echo __('Subjects'); ?></h2>
    <table cellpadding="0" cellspacing="0">
	<?php foreach ($subjects as $subject): ?>
        <tr>
            <td><?php echo h($subject['Subject']['subject_name']); ?>&nbsp;</td>
        </tr>
<?php endforeach; ?>
    </table>

</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Enroll New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('New Students Payment'), array('controller' =>'payment_details', 'action' => 'viewdetails')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Class'), array('controller' => 'studclasses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('Add New Subject'), array('controller' => 'subjects','action' => 'add')); ?></li>
        <br />
        <li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Payment Details'), array('controller' =>'payment_details','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Students Class'), array('controller' => 'studclasses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects','action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('View Particular Student'), array('controller' => 'payment_details', 'action' => 'viewparticular')); ?> </li>
        <br />
<!--        <li><?php echo $this->Html->link(__('Make Attendance'), array('controller' => 'attendances', 'action' => 'make')); ?> </li>
        <li><?php echo $this->Html->link(__('View Attendance'), array('controller' => 'attendances', 'action' => 'index')); ?> </li>-->
    </ul>
</div>
