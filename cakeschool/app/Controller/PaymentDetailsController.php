<?php

App::uses('AppController', 'Controller');

/**
 * PaymentDetails Controller
 *
 * @property PaymentDetail $PaymentDetail
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property SessionComponent $Sessionl
 */
class PaymentDetailsController extends AppController {

    // To be able to access student table also.  S
    // Set the first Table name to the one that corresponds to 
    // the this Controller (PaymentDetailsController)
    var $uses = array('PaymentDetail', 'Student');

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
      //   $this->PaymentDetail->virtualFields['closestdate'] = "ABS(DATEDIFF(PaymentDetail.date, NOW()))";
        $this->paginate = array('order' => 'PaymentDetail.paymentID DESC');
        $this->PaymentDetail->recursive = 0;
        $this->set('paymentDetails', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->PaymentDetail->exists($id)) {
            throw new NotFoundException(__('Invalid payment detail'));
        }
        $options = array('conditions' => array('PaymentDetail.' . $this->PaymentDetail->primaryKey => $id));
        $this->set('paymentDetail', $this->PaymentDetail->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function viewdetails() {
        if ($this->request->is('post')) {
            $studentroll = $this->request->data['Student']['roll_no'];
            $status = $this->Student->find('first', array(
                'conditions' => array(
                    'Student.roll_no' => $studentroll
            )));
            return $this->set('studentPayments', $status);
        }
        $this->set('studentPayments', '0');
    }

    public function add() {
        if ($this->request->is('post')) {
//            debug($this->request->data);
            $this->PaymentDetail->create();
            if ($this->PaymentDetail->save($this->request->data['Paymentdetail'])) {
                $this->Session->setFlash(__('The payment detail has been saved.'));
            } else {
                $this->Session->setFlash(__('The payment detail could not be saved. Please, try again.'));
            }
        }
    }

    public function viewparticular() {
        if ($this->request->is('post')) {

            $rollNumber = $this->request->data['Student']['roll_no'];
            $status = $this->Student->find('first', array(
                'conditions' => array(
                    'Student.roll_no' => $rollNumber
            )));
            return $this->set('studentDetails', $status);
        }
        $this->set('studentDetails', '0');
    }

}
