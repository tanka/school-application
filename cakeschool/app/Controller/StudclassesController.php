<?php

App::uses('AppController', 'Controller');

/**
 * Studclasses Controller
 *
 * @property Studclass $Studclass
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property SessionComponent $Session
 */
class StudclassesController extends AppController {

    var $use = array('Studclass', 'Teacher', 'Subject');

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Studclass->recursive = 0;
        $this->set('studclasses', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Studclass->exists($id)) {
            throw new NotFoundException(__('Invalid studclass'));
        }
        $options = array('conditions' => array('Studclass.' . $this->Studclass->primaryKey => $id));
        $this->set('studclass', $this->Studclass->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Studclass->create();
            if ($this->Studclass->save($this->request->data)) {
                $this->Session->setFlash(__('The Student Class has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Student Calss could not be saved. Please, try again.'));
            }
        }
        $teachers = $this->Studclass->Teacher->find('list', array('fields' => 'Teacher.teacher_name'));
        $this->set(compact('teachers'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //debug($this->request->is('post','put'));
        if ($this->request->is('post', 'put')) {
            $arryOfSubjects = array();
            $subjectList = $this->request->data['Studclass'];
            $subjectNames = array_keys($subjectList);
//            debug($subjectList);
//            die();
            $class = $this->Studclass->find('first', array('conditions' => array('Studclass.' . $this->Studclass->primaryKey => $subjectList['classID'])));
            $class['Studclass']['class_name'] = $subjectList['class_name'];
            $class['Studclass']['class_teacherID'] = $subjectList['class_teacherID'];
            $class['Studclass']['fees'] = $subjectList['fees'];
//            debug($subjectList);
//            die();
            for ($i = 0; $i < count($subjectNames); ++$i) {
                //print $subjectNames[$i];

                if ($subjectNames[$i] === 'classID' || $subjectNames[$i] === 'class_name' || $subjectNames[$i] === 'fees' ||
                        $subjectNames[$i] === 'class_teacherID') {
                    //print_r("same as classID");
                } else {
                    $subject_id = $subjectList[$subjectNames[$i]];

                    if ($subject_id != 0) {
                        //print_r($subject_id);

                        $subject = $this->Studclass->Subject->find('first', array('conditions' => array('Subject.' . $this->Studclass->Subject->primaryKey => $subject_id)));


                        $savingData['Studclass'] = $class['Studclass'];
                        array_push($arryOfSubjects, $subject['Subject']);
                    }
                }
            }
            $savingData['Subject'] = $arryOfSubjects;
            if ($this->Studclass->saveAll($savingData)) {
                $this->Session->setFlash(__('Class details and subjects edited.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                
            }
        } else {
            $options = array('conditions' => array('Studclass.' . $this->Studclass->primaryKey => $id));
            $this->set('studclass', $this->Studclass->find('first', $options));
        }
        $this->set('teachers', $this->Studclass->Teacher->find('list', array('fields' => 'Teacher.teacher_name')));
        $this->set('subjects', $this->Studclass->Subject->find('all'));

//        debug($this->request->data);
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Studclass->id = $id;
        if (!$this->Studclass->exists()) {
            throw new NotFoundException(__('Invalid studclass'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Studclass->delete()) {
            $this->Session->setFlash(__('The studclass has been deleted.'));
        } else {
            $this->Session->setFlash(__('The studclass could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void

      public function edit($id = null) {
      if (!$this->Studclass->exists($id)) {
      throw new NotFoundException(__('Invalid student'));
      }
      if ($this->request->is(array('post', 'put'))) {
      debug($this->request->data);
      if ($this->Studclass->save($this->request->data['Studclass'])) {
      $this->Session->setFlash(__('The student has been saved.'));
      return $this->redirect(array('action' => 'index'));
      } else {
      $this->Session->setFlash(__('The class could not be saved. Please, try again.'));
      }
      } else {
      $options = array('conditions' => array('Studclass.' . $this->Studclass->primaryKey => $id));
      $this->request->data = $this->Studclass->find('first', $options);
      }
      $teachers = $this->Studclass->Teacher->find('list', array(
      'fields' => 'Teacher.teacher_name',
      ));
      $this->set(compact('teachers'));

      $subjects = $this->Studclass->Subject->find('list', array(
      'fields' => 'Subject.subject_name',
      ));
      $this->set(compact('subjects'));
      }
      if (!$this->Studclass->exists($id)) {
      throw new NotFoundException(__('Invalid studclass'));
      }
      $options = array('conditions' => array('Studclass.' . $this->Studclass->primaryKey => $id));
      $this->set('studclass', $this->Studclass->find('first', $options));

      $this->set('subjects', $this->Studclass->Subject->find('list',array(
      'fields' => 'Subject.subject_name',
      )));




      }
     */
}
