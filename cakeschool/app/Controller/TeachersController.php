<?php
App::uses('AppController', 'Controller');

class TeachersController extends AppController {
    
    public $components = array('Paginator', 'Session'); 
    
    var $uses = array('Teacher', 'Studclass');
    
    public function index() {
		$this->Teacher->recursive = 0;
		$this->set('teachers', $this->Paginator->paginate());
	}
    
    public function add() {
		if ($this->request->is('post')) {
			$this->Teacher->create();
			if ($this->Teacher->save($this->request->data)) {
				$this->Session->setFlash(__('The teacher has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The teacher could not be saved. Please, try again.'));
			}
		}
                
	}
        
        public function view($id = null) {
        if (!$this->Teacher->exists($id)) {
            throw new NotFoundException(__('Invalid payment detail'));
        }
        $options = array('conditions' => array('Teacher.' . $this->Teacher->primaryKey => $id));
        $this->set('teacher', $this->Teacher->find('first', $options));

        }        
 /**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Teacher->exists($id)) {
			throw new NotFoundException(__('Invalid Teacher'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Teacher->save($this->request->data)) {
				$this->Session->setFlash(__('The Teacher has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Teacher could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Teacher.' . $this->Teacher->primaryKey => $id));
			$this->request->data = $this->Teacher->find('first', $options);
		}
	}

}
