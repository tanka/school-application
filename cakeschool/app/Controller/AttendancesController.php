<?php
App::uses('AppController', 'Controller');
/**
 * Attendances Controller
 *
 * @property Attendance $Attendance
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property SessionComponent $Session
 */
class AttendancesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
//         public function index() {
//        $this->Attendance->recursive = 0;
//            $this->set('attendance', $this->Paginator->paginate());
//            $attendances = $this->Attendance->find('all');
//            //debug($attendances);
//            $this->set('attendances', $attendances);
//    }
    
        
	public function index() {
          if ($this->request->is('post')) {
                                      
            $attended = $this->request->data['Attendance']['attendance_date'];
            
            $attend = $this->Attendance->find('all', array(
                'conditions' => array(
                    /* @var $attended type */
                    'Attendance.attendance_date' => date($attended['year'].'-'.$attended['month'].'-'.$attended['day'])
                    )));
             
            return $this->set('attendances', $attend);
            
           
        }else {
               $this->set('attendance', $this->Paginator->paginate());

            $attendances = $this->Attendance->Teacher->find('all');
            // debug($attendances);
            return $this->set('attendances', $attendances);
        }
   //     $this->set('attendances', '0');
        
    }

/**
 * make method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function make() {
        if ($this->request->is('post')) {
            foreach ($this->request->data['Attendance'] as $datas) {
//        //        debug($datas);
          
                if($this->Attendance->saveAll($datas)){
                   $this->Session->setFlash('Attendance has been saved.');
                 
                } else {
                    $this->Session->setFlash('Unable to add attendance.');
                   
                }
            }
            $this->redirect(array('action' => 'index')); 
        } 
        
        else {

            $this->set('attendance', $this->Paginator->paginate());

            $attendances = $this->Attendance->Teacher->find('all');
            // debug($attendances);
            $this->set('attendances', $attendances);
        }
    }
}
