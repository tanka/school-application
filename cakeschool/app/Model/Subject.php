<?php
App::uses('AppModel', 'Model');
/**
 * Subject Model
 *
 */
class Subject extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'subject';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'subjectID';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'subject_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'subjectID' => array(
			'blank' => array(
				'rule' => 'blank',
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'subject_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
                    'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Subject name already exists.',
                    ) 
		),
	);
        public $hasAndBelongsToMany = array(
            'Studclass' => array(
                'className' => 'Studclass',
                'foreignKey' => 'subjectID',
                'joinTable' => 'subject_class',
                'associationForeignKey' => 'studclassID',
            )
        );
}


