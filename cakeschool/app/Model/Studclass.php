<?php

App::uses('AppModel', 'Model');

/**
 * Stdclass Model
 *
 */
class Studclass extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'studclass';

    /**
     * Primary key field
     *
     * @var string
     */
    public $primaryKey = 'classID';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'class_name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'classID' => array(
            'blank' => array(
                'rule' => 'blank',
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'class_teacher' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'class_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Class Name Already Exists'
            )
        ),
        'fees' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
    public $belongsTo = array(
        'Teacher' => array(
            'className' => 'Teacher',
            'foreignKey' => 'class_teacherID',
        )
       
    );
    public $hasMany = array(
        'StudentsClass' => array(
            'className' => 'Student',
            'foreignKey' => 'student_classID',
        ));
    public $hasAndBelongsToMany = array(
            'Subject' => array(
                'className' => 'Subject',
                'foreignKey' => 'studclassID',
                'joinTable' => 'subject_class',
                'associationForeignKey' => 'subjectID',
            )
        );

}
